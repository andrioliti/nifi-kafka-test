const Kafka = require('no-kafka');

(() => {
    const producer = new Kafka.Producer();

    const time = 9999999999999;
    let cn = 0;

    const sendMessage = () => {
        cn = cn + 1;
        if (cn < time) {
            // console.log('Enviado msg ' + cn);

            producer.init().then(function() {
                producer.send({
                    topic: 'js',
                    message: {
                        value: Math.random(),
                    },
                });
            });

            setTimeout(sendMessage, 0);
        }

        return;
    }

    sendMessage();
})();